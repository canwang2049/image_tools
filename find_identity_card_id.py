#!/usr/bin/env python3

import re
import argparse
import datetime

g_current_year = date = datetime.date.today().year

g_need_male = None


def check_gender(identity_id):
    if g_need_male is None:
        return True
    # Assuming the 17th digit indicates gender: odd for male, even for female
    gender_digit = int(identity_id[16])
    return gender_digit % 2 != 0 and g_need_male


def check_if_date_valid(id):
    year = int(id[6:10])
    month = int(id[10:12])
    day = int(id[12:14])
    if 1900 <= year <= g_current_year and 1 <= month <= 12 and 1 <= day <= 31:
        return True
    return False


def check_if_identity_valid(identity_id):
    if not re.match(r"^\d{17}(\d|X)$", identity_id):
        return False

    if not check_gender(identity_id):
        return False

    if not check_if_date_valid(identity_id):
        return False

    identity_id = identity_id.upper()
    weights = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2]
    check_codes = ["1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2"]

    sum_weights = sum(int(identity_id[i]) * weights[i] for i in range(17))
    check_code = check_codes[sum_weights % 11]

    return check_code == identity_id[-1]


def generate_combinations(template, index=0):
    if index == len(template):  # Base case: all characters have been processed
        if check_if_identity_valid(template):
            print(template)
        return

    if template[index] == "*":
        for char in "0123456789X":
            # Replace "*" with each possible character and recurse
            generate_combinations(
                template[:index] + char + template[index + 1 :], index + 1
            )
    else:
        # If current character is not "*", move to the next character
        generate_combinations(template, index + 1)


def find_identity_id(template: str):
    if len(template) != 18:
        print("ERROR: Identity Card Id Template length must be 18")
        return

    if g_need_male is not None:
        if g_need_male:
            print("Show male only")
        else:
            print("Show female only")
    generate_combinations(template, 0)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Find identity ID")
    parser.add_argument(
        "id_template",
        type=str,
        help="Identity ID template. Fill the unknow with `*`, like `*1234567890123*`",
    )
    parser.add_argument(
        "--gender",
        type=bool,
        default=None,
        help="Specify the gender you need: 1/True for male, 0/False for female; None for all genders",
    )

    args = parser.parse_args()
    print(args)
    g_need_male = args.gender
    find_identity_id(args.id_template)
